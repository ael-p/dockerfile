FROM debian
COPY . /app
RUN apt-get update
RUN apt-get -y intall openjdk-8-jdk ssh vim
CMD [“java”, “-jar”, “/app/target/app.jar”]
